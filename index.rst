.. oep documentation master file, created by
   sphinx-quickstart on Sat Apr 16 12:18:53 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to oep's documentation!
============================================================

Contents:

.. toctree::
   :maxdepth: 1

   oep-0001
   oep-0002	      
   oep-0003
   oep-0004

Indices and tables
------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

