# -*- coding: utf-8 -*-

# Original authors:
# David Goodger <goodger@python.org>,
# Barry Warsaw <barry@python.org>


title_length = 55
column_format = (u' %(type)1s%(status)1s %(number)4s  %(title)-' +
                 u'%d' % title_length + u's %(authors)-s')

header = u"""OEP: 0
Title: Index of OER Enhancement Proposals (OEPs)
Last-Modified: %s
Author: OER Developers
Status: Active
Type: Informational
Created: 14-Apr-2016
"""

intro = u"""
    The OEP contains the index of all OER Enhancement Proposals,
    known as OEPs.  OEP numbers are assigned by the OEP Editor, and
    once assigned are never changed.  The GIT history[1] of the OEP
    texts represent their historical record.

"""

references = u"""
    [1] View OEP history online
        https://github.com/ros-infrastructure/oep
"""

footer = u"""
Local Variables:
mode: indented-text
indent-tabs-mode: nil
sentence-end-double-space: t
fill-column: 70
coding: utf-8
End:"""
