#!/usr/bin/env python
"""Auto-generate OEP 0 (OEP index).

Generating the OEP index is a multi-step process.  To begin, you must first
parse the OEP files themselves, which in and of itself takes a couple of steps:

    1. Parse metadata.
    2. Validate metadata.

With the OEP information collected, to create the index itself you must:

    1. Output static text.
    2. Format an entry for the OEP.
    3. Output the OEP (both by category and numerical index).

"""
from __future__ import absolute_import
from __future__ import print_function
from __future__ import with_statement

import sys
import os
import codecs

from operator import attrgetter

from oep0.output import write_oep0
from oep0.oep import OEP, OEPError


def main(argv):
    if not argv[1:]:
        path = '.'
    else:
        path = argv[1]

    oeps = []
    if os.path.isdir(path):
        for file_path in os.listdir(path):
            abs_file_path = os.path.join(path, file_path)
            if not os.path.isfile(abs_file_path):
                continue
            if file_path.startswith("oep-") and \
                    file_path.endswith(".rst") and \
                    not file_path == 'oep-0000.rst':
                with codecs.open(
                        abs_file_path, 'r', encoding='UTF-8') as oep_file:
                    try:
                        oep = OEP(oep_file)
                        if oep.number != int(file_path[4:-4]):
                            raise OEPError('OEP number does not match file ' +
                                           'name', file_path, oep.number)
                        oeps.append(oep)
                    except OEPError as e:
                        errmsg = "Error processing OEP %s (%s), excluding:" % \
                            (e.number, e.filename)
                        print(errmsg, e, file=sys.stderr)
                        sys.exit(1)
        oeps.sort(key=attrgetter('number'))
    elif os.path.isfile(path):
        with open(path, 'r') as oep_file:
            oeps.append(OEP(oep_file))
    else:
        raise ValueError("argument must be a directory or file path")

    with codecs.open('oep-0000.rst', 'w', encoding='UTF-8') as oep0_file:
        
        write_oep0(oeps, oep0_file)

if __name__ == "__main__":
    main(sys.argv)
