## OER Enhancement Proposals ##

This repository is for the OER Enhancement Proposals (OEPs) which encapsulate the developers guide to OER software.  Each OEP is a sequentiality numbered reStructuredText (.rst) documents, e.g., oep-0001.rst.

The sphinx-generated HTML of the files is hosted on the readthedocs (RTD) website at...
http://oep.readthedocs.org/en/latest/index.html

### Dependencies ###
The reST files are just plain text files, so there are no hard dependencies.

However, it may be useful to generate the HTML locally on your machine in parallel with the RTD auto-generated HTML.  To do so requires Sphinx (I use v1.4.1).  Installation instructions are at http://www.sphinx-doc.org/en/stable/install.html  I used the the method described on the RTD website (http://docs.readthedocs.org/en/latest/getting_started.html)

```
#!restructuredtext

$ sudo pip install sphinx sphinx-autobuild
```

Also, the conf.py file uses the sphinx_rtd_theme, so you may want to install the theme like so...
```
sudo pip install sphinx_rtd_theme
```

### Workflow ###

There are two workflows that we imagine a developer would use.  

1. Local: You can clone the git repository and make changes to the .rst files locally on your machine.  You can (but do not need to) generate the HTML files locally by executing this command in the project root-where the conf.py file is located. 

          sphinx-build -b html . ./_build/

2. You may also use the bitbucket browser-based editor to edit the reST files directly within the repository. 

Either way when you push the files to the bitbucket repository the webhooks should cause the RTD HTML to be automatically regenerated (it takes a minute or two!)

### Why do it this way?  ###

In some ways, using a git repos and reStructuredText is a complicated solution to a simple problem.  The problem is that we 
want to develop a set of software standards - a developer's guide - for OER projects.  I would term this solution a 
**browser-assisted** solution which has some advantages over a **browser-dependent** solution such as a wiki.

Advantages:

* Git distributed version control so that...
  * Developers can work offline on their own working copy
  * Workflows of branch, merge, etc. can are available
* reStructureText so that documents are plain text and extremely portable
* Browser-based front-end provided by bitbucket 

Disadvantages:

* Using the tools (git and reStructureText) is more cumbersome that a browser-dependent wiki
  
### Known Issues ###
* Would be nice to automatically generate the index.rst file
* Would be nice if number of each OEP was automated.

### Attribution ###
The concept of OEPs is an adaptation of ROS enhancement proposals (REPs) which is an adaptation of Python enhancement proposals (PEPs).  The tools for index and HTML generation are modified versions of these original tools.

### Who do I talk to? ###

* Andy ;)